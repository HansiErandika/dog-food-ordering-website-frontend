import { UserModel } from './../model/user.model';
import { apiUrl } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminLoginService {

  constructor(private http: HttpClient) { }

  signup(adminObj :any){
    return this.http.post<any>(apiUrl + 'Login/signup', adminObj);
  }

  update(adminObj :any){
    return this.http.put<any>(apiUrl + 'Login/update/'+adminObj.id, adminObj);
  }

  login(adminObj :any){
    return this.http.post<any>(apiUrl + 'Login/login', adminObj);
  }

  getadmins(){
    return this.http.get<UserModel[]>(apiUrl + 'Login/users')
  }

  getadmin(id: number){
    return this.http.get<UserModel>(apiUrl + 'Login/user/'+id)
  }

  deleteAdmin(id:number){
    return this.http.delete<any>(apiUrl+'Login/delete/'+id)
  }

  uploadfile(formData :any, id:number){
    return this.http.post<any>(apiUrl+'Login/ImageUpload/'+id, formData)
  }
}
