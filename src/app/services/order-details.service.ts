import { ContactModel } from './../model/contact.model';
import { apiUrl } from './../../environments/environment';
import { FoodDetailModel } from './../model/food-detail.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderDetailsService {

  constructor(private http: HttpClient) { }

  getFoodDetails(): Observable<FoodDetailModel[]> {
    return this.http.get<FoodDetailModel[]>(apiUrl + 'foodDetail');
  }

  getFoodDetail(id: number): Observable<FoodDetailModel> {
    return this.http.get<FoodDetailModel>(apiUrl + 'foodDetail/'+id);
  }

  createContact(contact: ContactModel): Observable<ContactModel>{
    return this.http.post<ContactModel>(apiUrl + 'contact', contact);
  }

}
