import { OrderDetailsService } from './../../services/order-details.service';
import { FoodDetailModel } from './../../model/food-detail.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu-page.component.html',
  styleUrls: ['./menu-page.component.css']
})
export class MenuPageComponent implements OnInit {

  getId: any;
  menuData:FoodDetailModel;

  constructor( private param: ActivatedRoute,
    private foodDetailService: OrderDetailsService) { }

  ngOnInit(): void {
    this.getId= this.param.snapshot.paramMap.get('id');

    this.getFoodDetails()
  }

  getFoodDetails(){
    this.foodDetailService.getFoodDetail(this.getId).subscribe((response)=>{
      this.menuData=response
    });
  }

}
