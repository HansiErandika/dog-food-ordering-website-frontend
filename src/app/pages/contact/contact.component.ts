import { OrderDetailsService } from './../../services/order-details.service';
import { ContactModel } from './../../model/contact.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contact= new ContactModel();

  constructor(private _service: OrderDetailsService) { }

  ngOnInit(): void {
  }

  createContact(){
    this._service.createContact(this.contact).subscribe((response)=>{
      
      alert("Thank you for contacting us!")
      this.contact=new ContactModel();
    })
  }

}
