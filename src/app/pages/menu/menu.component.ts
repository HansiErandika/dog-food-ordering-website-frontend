import { OrderDetailsService } from './../../services/order-details.service';
import { FoodDetailModel } from './../../model/food-detail.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  foodData: FoodDetailModel[];
  constructor(private foodDetailService: OrderDetailsService) { }

  ngOnInit(): void {
    this.getFoodDetails()
  }

  getFoodDetails(){
    this.foodDetailService.getFoodDetails().subscribe((response)=>{
      this.foodData=response
    });
  }

  

}
