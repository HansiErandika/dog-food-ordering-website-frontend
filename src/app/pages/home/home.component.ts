import { OrderDetailsService } from './../../services/order-details.service';
import { FoodDetailModel } from './../../model/food-detail.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  foodData: FoodDetailModel[];
  constructor(private foodDetailService: OrderDetailsService) { }

  ngOnInit(): void {
    this.getFoodDetails()
  }

  getFoodDetails(){
    this.foodDetailService.getFoodDetails().subscribe((response)=>{
      this.foodData=response
    });
  }


}
