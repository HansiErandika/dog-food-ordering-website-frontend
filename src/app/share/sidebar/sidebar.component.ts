import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
adminArrow = false;
productArrow = false;
categoryArrow = false;
user:string| null;
  showMenu: string = '';
	addExpandClass(element: any) {
		if (element === this.showMenu) {
			this.showMenu = '0';
		} else {
			this.showMenu = element;
		}
	}

  constructor() { }

  ngOnInit(): void {
    this.user=localStorage.getItem("name");
  }

  changeAdminArrow(){
    this.adminArrow = !this.adminArrow;
  }

  changeProductArrow(){
    this.productArrow = !this.productArrow;
  }

  changeCategoryArrow(){
    this.categoryArrow = !this.categoryArrow;
  }

  
}
