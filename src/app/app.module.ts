import { AdminDeleteComponent } from './admin/auth/admin-delete/admin-delete.component';
import { OrderDetailsService } from './services/order-details.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './share/navbar/navbar.component';
import { FooterComponent } from './share/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { MenuComponent } from './pages/menu/menu.component';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { MenuPageComponent } from './pages/menu-page/menu-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './admin/login/login.component';
import { SignupComponent } from './admin/signup/signup.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AdminlayoutComponent } from './includes/adminlayout/adminlayout.component';
import { LayoutComponent } from './includes/layout/layout.component';
import { SidebarComponent } from './share/sidebar/sidebar.component';
import { AdminHeaderComponent } from './share/admin-header/admin-header.component';
import { AdminFooterComponent } from './share/admin-footer/admin-footer.component';
import { ProductsViewComponent } from './admin/products/products-view/products-view.component';
import { AdminEditComponent } from './admin/auth/admin-edit/admin-edit.component';
import { JwtModule } from '@auth0/angular-jwt';
import { AuthGuard } from './guards/auth.guard';
import { AdminViewComponent } from './admin/auth/admin-view/admin-view.component';
import { ViewAllAdminComponent } from './admin/auth/view-all-admin/view-all-admin.component';

export function tokenGetter() { 
  return localStorage.getItem("jwt"); 
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    MenuComponent,
    AboutComponent,
    ContactComponent,
    MenuPageComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    LayoutComponent,
    AdminlayoutComponent,
    SidebarComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    ProductsViewComponent,
    AdminEditComponent,
    AdminViewComponent,
    AdminDeleteComponent,
    ViewAllAdminComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ["localhost:51190"],
        disallowedRoutes: []
      }
    })
  ],
  providers: [
    OrderDetailsService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
