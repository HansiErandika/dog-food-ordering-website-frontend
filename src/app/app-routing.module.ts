import { AdminDeleteComponent } from './admin/auth/admin-delete/admin-delete.component';
import { ViewAllAdminComponent } from './admin/auth/view-all-admin/view-all-admin.component';
import { AdminEditComponent } from './admin/auth/admin-edit/admin-edit.component';
import { LayoutComponent } from './includes/layout/layout.component';
import { MenuPageComponent } from './pages/menu-page/menu-page.component';
import { ContactComponent } from './pages/contact/contact.component';
import { MenuComponent } from './pages/menu/menu.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { LoginComponent } from './admin/login/login.component';
import { SignupComponent } from './admin/signup/signup.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { AdminlayoutComponent } from './includes/adminlayout/adminlayout.component';
import { ProductsViewComponent } from './admin/products/products-view/products-view.component';
import { AuthGuard } from './guards/auth.guard';
import { AdminViewComponent } from './admin/auth/admin-view/admin-view.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'menu', component: MenuComponent },
      { path: 'menu/:id', component: MenuPageComponent },
      { path: 'about', component: AboutComponent },
      { path: 'contact', component: ContactComponent },
    ]
  },
  {
    path: 'admin',
    component: AdminlayoutComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
      { path: 'view-product', component: ProductsViewComponent, canActivate: [AuthGuard] },
      { path: 'signup', component: SignupComponent, canActivate: [AuthGuard] },
      { path: 'edit-admin/:id', component: AdminEditComponent, canActivate: [AuthGuard] },
      { path: 'create-admin', component: AdminEditComponent, canActivate: [AuthGuard] },
      { path: 'view-admin/:id', component: AdminViewComponent, canActivate: [AuthGuard] },
      { path: 'delete-admin/:id', component: AdminDeleteComponent, canActivate: [AuthGuard] },
      { path: 'view-all-admin', component: ViewAllAdminComponent, canActivate: [AuthGuard] },
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  
  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
