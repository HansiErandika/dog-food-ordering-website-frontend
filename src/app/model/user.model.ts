export class UserModel{
    id: number;
    name: string;
    email: string;
    mobileNumber: string;
    password: string;
    status: number;
    image: string;
}