export class FoodDetailModel{
    id:number;
    foodName: string;
    foodDetails:string;
    price: number;
    foodImg:string;
}