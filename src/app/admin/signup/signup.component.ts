import { UserModel } from './../../model/user.model';
import { AdminLoginService } from './../../services/admin-login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupform: FormGroup;
  public signupObj = new UserModel();

  constructor(private formbuilder: FormBuilder,
    private router: Router,
    private _loginService: AdminLoginService) { }

  ngOnInit(): void {

    this.signupform = this.formbuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobileNumber: ['', Validators.required],
      password: ['', Validators.required],
      status: [1],
    });
  }

  signup() {
    this.signupObj.name= this.signupform.value.name;
    this.signupObj.email= this.signupform.value.email;
    this.signupObj.mobileNumber= this.signupform.value.mobileNumber;
    this.signupObj.password= this.signupform.value.password;
    this.signupObj.status= this.signupform.value.status;

    this._loginService.signup(this.signupObj)
      .subscribe(res => {
        alert(res.message);
        this.signupform.reset();
        this.router.navigate(['login'])
      });

  }

}