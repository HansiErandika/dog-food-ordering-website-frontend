import { AdminLoginService } from './../../services/admin-login.service';
import { Router } from '@angular/router';
import { UserModel } from './../../model/user.model';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';
import { NavbarComponent } from './../../share/navbar/navbar.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  public signupObj = new UserModel();
  invalidLogin: boolean;

  constructor(private formbuilder: FormBuilder,
    private router: Router,
    private _loginService: AdminLoginService) { }

  ngOnInit(): void {

    this.loginForm = this.formbuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  login() {

    this.signupObj.email = this.loginForm.value.email;
    this.signupObj.password = this.loginForm.value.password;


    this._loginService.login(this.signupObj)
      .subscribe(res => {
        if(res.statusCode==200){
          const token = res.jwtToken;
          localStorage.setItem("jwt", token);
          localStorage.setItem("name", res.userData);
          this.invalidLogin=false;
          this.loginForm.reset();
          this.router.navigate(['admin/dashboard'])
        }
        else{
          
          alert(res.message)
        }
        
      });


  }


}
