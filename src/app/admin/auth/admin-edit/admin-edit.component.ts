import { AdminLoginService } from './../../../services/admin-login.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UserModel } from './../../../model/user.model';
import { Component, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { apiUrl } from 'src/environments/environment';
// import { EventEmitter } from 'stream';

@Component({
  selector: 'app-admin-edit',
  templateUrl: './admin-edit.component.html',
  styleUrls: ['./admin-edit.component.css']
})
export class AdminEditComponent implements OnInit {

  user: UserModel;
  url: any;
  urluploded: any;
  fileInput: any;
  msg = "";
  message: string;
  progress: number;
  adminUserForm: FormGroup;
  adminId: number;
  imageFile: File;
  // public imageName: string;

  formData = new FormData();

  // @Output() public onUploadFinished = new EventEmitter();

  constructor(private formbuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private adminService: AdminLoginService) { }

  ngOnInit(): void {
    this.url = '../../../../assets/uploads/admins/default.png';
    this.adminUserForm = this.formbuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobileNumber: ['', Validators.required],
      password: ['', Validators.required],
      status: [1],
      // image: '',
    });

    this.activatedRoute.params.subscribe(params => {
      const userId = params['id'];
      this.adminId = userId;
      if (userId > 0 && this.adminId != undefined) {
        this.getadmin(userId);
        this.adminService.getadmin(userId)

          .subscribe((response: any) => {
            // response = this.adminUserForm.patchValue(response)
            this.adminUserForm.setValue({
              name: response.name,
              email: response.email,
              mobileNumber: response.mobileNumber,
              password: response.password,
              status: response.status,
              // image: response.image,
            });
            if (response.image != null)
              this.url = response.image;
          });
      }

    });


  }

  getadmin(id: number) {
    this.adminService.getadmin(id).subscribe((response: UserModel) => {
      this.user = response;
      if (response.image != null)
        this.url = response.image;
    });
  }

  update() {
    if (this.adminId > 0 && this.adminId != undefined) {
      this.user.name = this.adminUserForm.value.name;
      this.user.email = this.adminUserForm.value.email;
      this.user.mobileNumber = this.adminUserForm.value.mobileNumber;
      this.user.password = this.adminUserForm.value.password;
      this.user.status = this.adminUserForm.value.status;

      this.adminService.update(this.user)
        .subscribe(res => {
          alert(res.message);
          this.adminUserForm.reset();
          this.router.navigate(['admin/view-all-admin'])
        });
    }
    else {
      // console.log(this.user)
      this.adminService.signup(this.adminUserForm.value)
        .subscribe(res => {
          alert(res.message);
          this.adminUserForm.reset();
          this.router.navigate(['admin/view-all-admin'])
        });
    }
    if (this.formData != null) {

      this.adminService.uploadfile(this.formData, this.adminId)
        .subscribe((res) => {
          // alert('ok')
          this.url = res.filename;
          // this.imageName = res.imageName;
        })
    }
  }



  onFileChanged(event: any) {
    if (!event.target.files[0] || event.target.files[0].length == 0) {
      this.msg = 'You must select an image';
      return;
    }

    var mimeType = event.target.files[0].type;

    if (mimeType.match(/image\/*/) == null) {
      this.msg = "Only images are supported";
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);

    reader.onload = (_event) => {
      this.msg = "";
      this.url = reader.result;
    }
    let file = event.target.files[0];

    this.formData.append('file', file, file.name)


    // const file = event.target.files[0]
  }

  removeImage() {
    console.log('gfgjsfs')
  }
}
// {
//   name: new FormControl(''),
//   email: new FormControl(''),
//   password: new FormControl(''),
//   conform_password: new FormControl(''),
// }
