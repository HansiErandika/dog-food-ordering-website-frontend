import { AdminLoginService } from './../../../services/admin-login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from './../../../model/user.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {

  user: UserModel;
  adminId:number;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private adminService: AdminLoginService) { }


  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      const userId = params['id'];
      this.adminId= userId;
      if (userId > 0 && this.adminId != undefined) {
        this.getadmin(userId);
        
      }

    });
  }


  getadmin(id: number) {
    this.adminService.getadmin(id).subscribe((response: UserModel) => {
      this.user = response;
    });
  }
}
