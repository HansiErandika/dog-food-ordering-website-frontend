import { AdminLoginService } from './../../../services/admin-login.service';
import { UserModel } from './../../../model/user.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-all-admin',
  templateUrl: './view-all-admin.component.html',
  styleUrls: ['./view-all-admin.component.css']
})
export class ViewAllAdminComponent implements OnInit {

  users: UserModel[];
  constructor(private adminService: AdminLoginService) { }

  ngOnInit(): void {
    this.getadmins();
  }

  getadmins() {
    this.adminService.getadmins().subscribe((response) => {
      this.users = response
    });
  }

}
