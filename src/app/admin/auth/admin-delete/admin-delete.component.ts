import { AdminLoginService } from '../../../services/admin-login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from '../../../model/user.model';
import { Component, OnInit } from '@angular/core';
import { asLiteral } from '@angular/compiler/src/render3/view/util';

@Component({
  selector: 'app-admin-delete',
  templateUrl: './admin-delete.component.html',
  styleUrls: ['./admin-delete.component.css']
})
export class AdminDeleteComponent implements OnInit {

  user: UserModel;
  adminId:number;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private adminService: AdminLoginService) { }


  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      const userId = params['id'];
      this.adminId= userId;
      if (userId > 0 && userId != undefined) {
        this.getadmin(userId);
        
      }

    });
  }


  getadmin(id: number) {
    this.adminService.getadmin(id).subscribe((response: UserModel) => {
      this.user = response;
    });
  }

  deleteAdmin(id:number){
    this.adminService.deleteAdmin(id).subscribe((res:any)=>{
      alert(res.message)
      this.router.navigate(['admin/view-all-admin'])
    });
  }
}
